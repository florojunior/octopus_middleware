package br.com.transire.octopus.middleware.util;

public interface ConfigurationPropertyNames {

	public static final String SOCKET_PORT = "socket.port";
	
	public static final String CLP_IP_ADDRESS = "clp.ip.address";
	
	public static final String CLP_MEMORY_READ_CYCLE_TIME = "clp.memory.read.cycle.time";
	
	public static final String TREADMILL_TOTAL_TIME = "treadmill.total.time";
	
	public static final String OCTOPUS_WEB_SERVICE_URL = "octopus.web.service.url";
}
