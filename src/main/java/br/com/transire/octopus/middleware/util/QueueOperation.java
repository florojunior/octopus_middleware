package br.com.transire.octopus.middleware.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QueueOperation {

	private List<String> resources;
	
	
	public QueueOperation() {
		
		resources = Collections.synchronizedList(new ArrayList<String>());
	}
	
	public void init() {
		
		// Thread Produtora (insere na lista)
		new Thread(new Runnable() {
			public void run() {
				
				int cod = 0;
				while(true) {
					
					try { Thread.sleep(5000); } catch (InterruptedException e) {}
					
					System.out.println("[PRODUTOR] inseriu C�digo " + cod);
					
					resources.add("C�digo " + cod++);
					
					synchronized(resources) {
						resources.notifyAll();
						System.out.println("Produtor notifica que ha o que consumir");
					}
				}
			}
		}).start();
		
		// Thread Consumidora (remove da lista)
		new Thread(new Runnable() {
			public void run() {
				
				while(true) {
					
					if(!resources.isEmpty()) {
						
						String[] items = new String[resources.size()];
						
						int index = 0;
						for(String str : resources)
							items[index++] = str;
						
						//try { Thread.sleep(3500); } catch (InterruptedException e) {}
						
						String log = "";
						for(String str : items) {
							resources.remove(str);
							log +=  (!log.isEmpty() ? ", " : "") + str;
						}
						
						System.out.println("[CONSUMIDOR] Removeu {" + log + "} ainda existem " + resources.size() + " items a serem consumidos");
						
					} else {
						
						synchronized(resources) {
							try {
								System.out.println("Consumidor aguardando uma notificacao");
								resources.wait();
							} catch (InterruptedException e) {}
						}
					}
					
					
				}
			}
		}).start();
	}
	
	public static void main(String[] args) {
		
		QueueOperation qo = new QueueOperation();
		qo.init();
	}
}
