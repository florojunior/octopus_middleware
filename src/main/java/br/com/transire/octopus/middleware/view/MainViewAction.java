package br.com.transire.octopus.middleware.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import br.com.transire.octopus.middleware.client.OctopusWSClient;
import br.com.transire.octopus.middleware.core.CLPMemoryAccessController;
import br.com.transire.octopus.middleware.core.CLPSocketMessageHandler;
import br.com.transire.octopus.middleware.socket.MessageHandler;
import br.com.transire.octopus.middleware.socket.SocketCommunicator;
import br.com.transire.octopus.middleware.util.ConfigurationPropertyNames;
import br.com.transire.octopus.middleware.util.TextUtil;

public class MainViewAction extends MainView {

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -8405270772170014383L;
	
	
	private static final int MAX_LINES = 100;
	
	
	private String octopusWSURL = "http://localhost:8080/octopus";
	
	private int socketPort = 2235;
	
	private String clpIPAddress = "172.18.100.201";
	
	private int readCicleTime = 3000;
	
	private int treadmillTotalTime = 60000;
	
	
	private OctopusWSClient octopusWSClient;
	
	private CLPSocketMessageHandler socketMessageHandler;
	
	private SocketCommunicator socketCommunicator;
	
	private CLPMemoryAccessController clpMemoryAccessController;
	
	
	private String clpMemorySeparationRTI;
	
	private String clpMemoryChangeBoxRTI;
	
	private String clpMemoryPostStatusRTI;
	
	
	public MainViewAction() {
		
		addWindowListener(new WindowAdapter() {
			public void windowOpened(WindowEvent e) {
				MainViewAction.this.initialize();
			}
			
			public void windowClosed(WindowEvent e) {
				MainViewAction.this.closeResources();
			}
		});
	}

	
	private void initialize() {
		
		// Carrega o arquivo de Configuracoes
		try {
			readConfigurationProperties();
		} catch (Exception e) {
			
			JOptionPane.showMessageDialog(this,  
				"Erro durante a leitura das configurações iniciais do Middleware: " + e.getMessage(),
				"Octopus - Middleware",
				JOptionPane.ERROR_MESSAGE);
			
			System.exit(1);
		}
		
		lbSocketConfigInfo.setText("Port: " + socketPort);
		
		lbCLPMemConfigInfo.setText("CLP IP: " + clpIPAddress + " | Read Cicle Time: " + readCicleTime + " ms");
		
		lbOctWSConfigInfo.setText("URL: " + octopusWSURL);
		
		
		// Configura o client para comunicacao com o Web Service do Octopus
		octopusWSClient = configureOctopusWSClient(octopusWSURL);
		
		// Configura o Controlador de acesso a memoria da CLP
		clpMemoryAccessController = configureCLPMemoryReadController(
				clpIPAddress, readCicleTime, octopusWSClient);
		
		// Configura o Tratador de Mensagens recebidas da CLP via Socket
		socketMessageHandler = configureCLPSocketMessageHandler(
				octopusWSClient, clpMemoryAccessController, treadmillTotalTime);
		
		try {
			
			// Configura o Comunicador Socket para receber as mensagens da CLP
			socketCommunicator = configureSocketCommunicator(socketPort, socketMessageHandler);
			
			// Inicializa o Servidor Socket
			socketCommunicator.startServer();
			
		} catch (IOException e) {
			addSocketDestinationLog("Error starting Socket Server: " + e.getMessage());
		}
		
		
		// Inicializa o controlador de acesso a memoria da CLP
		clpMemoryAccessController.start();
		
		ckbCLPMemShowReadTimes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clpMemoryAccessController.setRegisterReadingTimesInLog(
					ckbCLPMemShowReadTimes.isSelected());
			}
		});
		
		ckbCLPMemShowReadTimes.setSelected(clpMemoryAccessController.isRegisterReadingTimesInLog());
		
		btCLPMemReconnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clpMemoryAccessController.start();
			}
		});
		
		tbCLPMem.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				switch(tbCLPMem.getSelectedIndex()) {
					case 0 : lbCLPMemStatus.setText(clpMemorySeparationRTI); break;
					case 1 : lbCLPMemStatus.setText(clpMemoryChangeBoxRTI); break;
					case 2 : lbCLPMemStatus.setText(clpMemoryPostStatusRTI); break;
				}
			}
		});
		
		btCLPMemTest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//clpMemoryAccessController.addNewChangeBox(1);
				
				for(int index = 1; index <= 36; index++) {
					
					clpMemoryAccessController.writePostCapacity(index, true);
					
					try {
						Thread.sleep(100);
					} catch (InterruptedException e1) {}
					
					clpMemoryAccessController.writePostCapacity(index, false);
				}
				
				/*
				for(int index = 1; index <= 36; index++) {
					
					clpMemoryAccessController.writePostCapacity(index, false);
					
					try {
						Thread.sleep(100);
					} catch (InterruptedException e1) {}
				}
				*/
			}
		});
		
		// Thread que verifica o tamanho dos textos de logs (10 min)
		new Timer().schedule(new TimerTask() {
			public void run() {
				checkTextSizeLogs(10);
			}
		}, 600000, 600000);
	}
	
	private void closeResources() {
		
		if(socketCommunicator != null)
			socketCommunicator.finishServer();
		
		if(clpMemoryAccessController != null)
			clpMemoryAccessController.finishMemoryRead();
	}
	
	
	/**
	 * Instancia o objeto client para comunicacao com o web service do Octopus e
	 * configura as mensagens de log para exibir no console do middleware
	 * 
	 * @param url
	 * @return
	 */
	private OctopusWSClient configureOctopusWSClient(String url) {
		
		return new OctopusWSClient(url) {
			
			protected void registerDebugLog(String message, int type) {
				
				super.registerDebugLog(message, type);
				
				switch(type) {
					case OctopusWSClient.DETERMINE_DESTINATION_MESSAGE_TYPE : addOctWSDestinationLog(message); break;
					case OctopusWSClient.CONFIRM_POST_RECEIPT_MESSAGE_TYPE : addOctWSConfirmationLog(message); break;
					case OctopusWSClient.RESET_POST_COUNTING_MESSAGE_TYPE : addOctWSResetPostLog(message); break;
					case OctopusWSClient.SET_POST_STATUS_MESSAGE_TYPE : addOctWSPostStatusLog(message); break;
				}
			}
			
			protected void registerErrorLog(String message, int type) {
				
				super.registerErrorLog(message, type);
				
				switch(type) {
					case OctopusWSClient.DETERMINE_DESTINATION_MESSAGE_TYPE : addOctWSDestinationLog(message); break;
					case OctopusWSClient.CONFIRM_POST_RECEIPT_MESSAGE_TYPE : addOctWSConfirmationLog(message); break;
					case OctopusWSClient.RESET_POST_COUNTING_MESSAGE_TYPE : addOctWSResetPostLog(message); break;
					case OctopusWSClient.SET_POST_STATUS_MESSAGE_TYPE : addOctWSPostStatusLog(message); break;
				}
			}
		};
	}
	
	/**
	 * Inicializa o objeto que realiza o tratamento das mensagens recebidas da CLP via protocolo
	 * Socket e redireciona para o web service do Octopus e configura as mensagens de log para
	 * serem exibidas no console do middleware
	 * 
	 * @param octopusClient
	 * @return
	 */
	private CLPSocketMessageHandler configureCLPSocketMessageHandler(
			OctopusWSClient octopusClient, CLPMemoryAccessController clpMemoryAccessControlles, int treadmillTotalTime) {
		
		return new CLPSocketMessageHandler(octopusClient, clpMemoryAccessControlles, treadmillTotalTime) {
			
			protected void registerDebugLog(String message) {
				super.registerDebugLog(message);
				addSocketDestinationLog(message);
			}
			
			protected void registerErrorLog(String message) {
				super.registerErrorLog(message);
				addSocketDestinationLog(message);
			}
			
			protected void displaySocketDestinationTimeIndicators(String indicators) {
				lbSocketStatus.setText(indicators);
			}
		};
	}
	
	/**
	 * Inicializa o objeto que implementa a comunicacao via socket
	 * e configura as mensagens de log para serem exibidas no console do middleware
	 * 
	 * @param port
	 * @param messageHandler
	 * @return
	 * @throws IOException
	 */
	private SocketCommunicator configureSocketCommunicator(int port, MessageHandler messageHandler) throws IOException {
		
		return new SocketCommunicator(port, messageHandler) {
			
			protected void registerDebugLog(String message) {
				super.registerDebugLog(message);
				addSocketDestinationLog(message);
			}
			
			protected void registerErrorLog(String message) {
				super.registerErrorLog(message);
				addSocketDestinationLog(message);
			}
		};
	}
	
	private CLPMemoryAccessController configureCLPMemoryReadController(String ipAddress, int readCicleTime, OctopusWSClient octopusWSClient) {
		
		return new CLPMemoryAccessController(ipAddress, readCicleTime, octopusWSClient) {
			
			protected void connected(String clpIPAddress, int port) {
				
				String msg = "CLP in " + clpIPAddress + " Connected";
				
				addCLPMemSeparationLog(msg);
				addCLPMemChangeBoxLog(msg);
				addCLPMemPostStatusLog(msg);
				
				btCLPMemReconnect.setVisible(false);
			}
			
			protected void failConnection(String clpIPAddress, String error) {
				
				String msg = "CLP in " + clpIPAddress + " Connection Error: " + error;
				
				addCLPMemSeparationLog(msg);
				addCLPMemChangeBoxLog(msg);
				addCLPMemPostStatusLog(msg);
				
				btCLPMemReconnect.setVisible(true);
			}
			
			protected void registerDebugLog(String message, int type) {
				
				super.registerDebugLog(message, type);
				
				switch(type) {
					case CLPMemoryAccessController.SEPARATION_MESSAGE_TYPE : addCLPMemSeparationLog(message); break;
					case CLPMemoryAccessController.CHANGE_BOX_MESSAGE_TYPE : addCLPMemChangeBoxLog(message); break;
					case CLPMemoryAccessController.POST_STATUS_MESSAGE_TYPE : addCLPMemPostStatusLog(message); break;
					case CLPMemoryAccessController.POST_CAPACITY_MESSAGE_TYPE : addCLPMemPostCapacityLog(message); break;
				}
			}
			
			protected void registerErrorLog(String message, int type) {
				
				super.registerErrorLog(message, type);
				
				switch(type) {
					case CLPMemoryAccessController.SEPARATION_MESSAGE_TYPE : addCLPMemSeparationLog(message); break;
					case CLPMemoryAccessController.CHANGE_BOX_MESSAGE_TYPE : addCLPMemChangeBoxLog(message); break;
					case CLPMemoryAccessController.POST_STATUS_MESSAGE_TYPE : addCLPMemPostStatusLog(message); break;
					case CLPMemoryAccessController.POST_CAPACITY_MESSAGE_TYPE : addCLPMemPostCapacityLog(message); break;
				}
			}
			
			protected void displaySeparationReadingTimesIndicators(String indicators) {
				clpMemorySeparationRTI = indicators;
				if(tbCLPMem.getSelectedIndex() == 0)
					lbCLPMemStatus.setText(indicators);
			}
			
			protected void displayChangeBoxReadingTimesIndicators(String indicators) {
				clpMemoryChangeBoxRTI = indicators;
				if(tbCLPMem.getSelectedIndex() == 1)
					lbCLPMemStatus.setText(indicators);
			}
			
			protected void displayPostStatusReadingTimesIndicators(String indicators) {
				clpMemoryPostStatusRTI = indicators;
				if(tbCLPMem.getSelectedIndex() == 2)
					lbCLPMemStatus.setText(indicators);
			}
		};
	}
	
	private void checkTextSizeLogs(int linesLimit) {
		
		checkTextAreaSize(txaSocketDestinationLog);
		
		checkTextAreaSize(txaCLPMemSeparationLog);
		checkTextAreaSize(txaCLPMemChangeBoxLog);
		checkTextAreaSize(txaCLPMemPostStatusLog);
		
		checkTextAreaSize(txaOctWSDestinationLog);
		checkTextAreaSize(txaOctWSConfirmationLog);
		checkTextAreaSize(txaOctWSResetPostLog);
		checkTextAreaSize(txaOctWSPostStatusLog);
	}
	
	private void checkTextAreaSize(JTextArea textArea) {
		
		int lines = TextUtil.countLines(textArea.getText());
		if(lines > MAX_LINES)
			textArea.setText(TextUtil.removeFirstLines(
				textArea.getText(), lines - MAX_LINES));
	}
	
	public String addSocketDestinationLog(String message) {
		txaSocketDestinationLog.append(strDateHour() + message + "\n");
		return message;
	}
	
	public String addCLPMemSeparationLog(String message) {
		txaCLPMemSeparationLog.append(strDateHour() + message + "\n");
		return message;
	}

	public String addCLPMemChangeBoxLog(String message) {
		txaCLPMemChangeBoxLog.append(strDateHour() + message + "\n");
		return message;
	}
	
	public String addCLPMemPostStatusLog(String message) {
		txaCLPMemPostStatusLog.append(strDateHour() + message + "\n");
		return message;
	}
	
	public String addCLPMemPostCapacityLog(String message) {
		txaCLPMemPostCapacityLog.append(strDateHour() + message + "\n");
		return message;
	}
	
	public String addOctWSDestinationLog(String message) {
		txaOctWSDestinationLog.append(strDateHour() + message + "\n");
		return message;
	}
	
	public String addOctWSConfirmationLog(String message) {
		txaOctWSConfirmationLog.append(strDateHour() + message + "\n");
		return message;
	}
	
	public String addOctWSResetPostLog(String message) {
		txaOctWSResetPostLog.append(strDateHour() + message + "\n");
		return message;
	}
	
	public String addOctWSPostStatusLog(String message) {
		txaOctWSPostStatusLog.append(strDateHour() + message + "\n");
		return message;
	}
	
	private void readConfigurationProperties() throws Exception {
		
		Properties properties = new Properties();
		InputStream propFile = null;
			
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		propFile = classLoader.getResourceAsStream("configuration.properties");
		
		properties.load(propFile);
		
		if(properties.containsKey(ConfigurationPropertyNames.SOCKET_PORT))
			this.socketPort = Integer.parseInt(properties.getProperty(ConfigurationPropertyNames.SOCKET_PORT).trim());
		else throw new Exception("Porta de Escuta do Protocolo Socket nao foi Configurada");
		
		if(properties.containsKey(ConfigurationPropertyNames.CLP_IP_ADDRESS))
			this.clpIPAddress = properties.getProperty(ConfigurationPropertyNames.CLP_IP_ADDRESS).trim();
		else throw new Exception("Endereco IP da CLP nao foi configurado");
		
		if(properties.containsKey(ConfigurationPropertyNames.CLP_MEMORY_READ_CYCLE_TIME))
			this.readCicleTime = Integer.parseInt(properties.getProperty(ConfigurationPropertyNames.CLP_MEMORY_READ_CYCLE_TIME).trim());
		else throw new Exception("Tempo do Ciclo de leitura da memoria da CLP nao foi configurado");
		
		if(properties.containsKey(ConfigurationPropertyNames.TREADMILL_TOTAL_TIME))
			this.treadmillTotalTime = Integer.parseInt(properties.getProperty(ConfigurationPropertyNames.TREADMILL_TOTAL_TIME).trim());
		else throw new Exception("Tempo total do percurso da esteira nao foi configurado");
		
		if(properties.containsKey(ConfigurationPropertyNames.OCTOPUS_WEB_SERVICE_URL))
			this.octopusWSURL = properties.getProperty(ConfigurationPropertyNames.OCTOPUS_WEB_SERVICE_URL).trim();
		else throw new Exception("URL de acesso ao Web Service do Octopus nao foi configurada");
	}
	
	private String strDateHour() {
		return new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss] ").format(new Date());
	}
}
