package br.com.transire.octopus.middleware.client;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import br.org.creathus.infra.integration.ServiceManager;

public class OctopusWSClient {

	
	private static final Logger logger = LogManager.getLogger(OctopusWSClient.class.getSimpleName());
	
	
	public static final int DETERMINE_DESTINATION_MESSAGE_TYPE = 1;
	
	public static final int CONFIRM_POST_RECEIPT_MESSAGE_TYPE = 2;
	
	public static final int RESET_POST_COUNTING_MESSAGE_TYPE = 3;
	
	public static final int SET_POST_STATUS_MESSAGE_TYPE = 4;
	
	
	private static final String DESTINATION_SERVICE_NAME = "DestinationService";
	
	private static final String ACCOUNTING_SERVICE_NAME = "AccountingService";
	
	
	// determineDestination(String[] params)
	private static final String DETERMINE_DESTINATION_METHOD_NAME = "determineDestinatination";
	
	// confirmPostReceipt(String packageId, int postNumber)
	private static final String CONFIRM_POST_RECEIPT_METHOD_NAME = "confirmPostReceipt";
	
	// resetPostCounting(int postNumber)
	private static final String RESET_POST_COUNTING_METHOD_NAME = "resetPostCounting";
	
	// setPostStatus(int postNumber, boolean active)
	private static final String SET_POST_STATUS_METHOD_NAME = "setPostStatus";
	
	
	private String url;
	
	
	public OctopusWSClient(String url) {
		this.url = url;
	}
	
	
	/**
	 * Envia os dados de um pacote identificado e recebe como retorno o numero do posto de destino
	 * 
	 * @param parameters
	 * @return
	 */
	public String determinateDestination(String[] parameters) {
		
		String response = null;
		
		try {
			
			registerDebugLog(
				sending(DESTINATION_SERVICE_NAME, DETERMINE_DESTINATION_METHOD_NAME, parameters), 
				DETERMINE_DESTINATION_MESSAGE_TYPE);
			
			response = ServiceManager.request(url, 
				DESTINATION_SERVICE_NAME, DETERMINE_DESTINATION_METHOD_NAME, 
				new Object[] {parameters}, String.class);
			
			registerDebugLog(
				received(DESTINATION_SERVICE_NAME, DETERMINE_DESTINATION_METHOD_NAME, parameters, response), 
				DETERMINE_DESTINATION_MESSAGE_TYPE);
			
		} catch(Throwable t) {
			
			registerErrorLog(
				fail(DESTINATION_SERVICE_NAME, DETERMINE_DESTINATION_METHOD_NAME, parameters, t.getMessage()),
				DETERMINE_DESTINATION_MESSAGE_TYPE);
			
			throw t;
		}
		
		return response;
	}
	
	/**
	 * Confirma o recebimento de um pacote no posto de destino
	 * 
	 * @param packageId
	 * @param postNumber
	 * @return
	 */
	public void confirmPostReceipt(String[][] confirmations) {
		
		String[] parameters = convertParams(confirmations);
		
		try {
			
			registerDebugLog(
				sending(ACCOUNTING_SERVICE_NAME, CONFIRM_POST_RECEIPT_METHOD_NAME, parameters),
				CONFIRM_POST_RECEIPT_MESSAGE_TYPE);
			
			ServiceManager.request(url, 
				ACCOUNTING_SERVICE_NAME, CONFIRM_POST_RECEIPT_METHOD_NAME, 
				new Object[] {confirmations});
			
			registerDebugLog(
				received(ACCOUNTING_SERVICE_NAME, CONFIRM_POST_RECEIPT_METHOD_NAME, parameters, "OK"),
				CONFIRM_POST_RECEIPT_MESSAGE_TYPE);
			
		} catch(Throwable t) {
			
			registerErrorLog(
				fail(ACCOUNTING_SERVICE_NAME, CONFIRM_POST_RECEIPT_METHOD_NAME, 
					parameters, t.getMessage()), CONFIRM_POST_RECEIPT_MESSAGE_TYPE);
			
			throw t;
		}
	}
	
	/**
	 * Informa ao Octopus que a caixa coletora de um posto foi trocada para zerar a contagem
	 * 
	 * @param postNumber
	 * @return
	 */
	public void resetPostCounting(Integer[] postNumbers) {
		
		try {
			
			registerDebugLog(
				sending(ACCOUNTING_SERVICE_NAME, RESET_POST_COUNTING_METHOD_NAME, postNumbers),
				RESET_POST_COUNTING_MESSAGE_TYPE);
			
			ServiceManager.request(url, 
					ACCOUNTING_SERVICE_NAME, RESET_POST_COUNTING_METHOD_NAME, 
				new Object[] {postNumbers});
			
			registerDebugLog(
				received(ACCOUNTING_SERVICE_NAME, RESET_POST_COUNTING_METHOD_NAME, postNumbers, "OK"),
				RESET_POST_COUNTING_MESSAGE_TYPE);
			
		} catch(Throwable t) {
			
			registerErrorLog(
				fail(ACCOUNTING_SERVICE_NAME, RESET_POST_COUNTING_METHOD_NAME, postNumbers, t.getMessage()),
				RESET_POST_COUNTING_MESSAGE_TYPE);
			
			throw t;
		}
	}

	/**
	 * Informa ao Octopus a ativacao ou desativacao de um determinado Posto
	 * 
	 * @param postNumber
	 * @param active
	 * @return
	 */
	public void setPostStatus(Integer[][] postStatus) {
		
		String[] parameters = convertParams(postStatus);
		
		try {
			
			registerDebugLog(
				sending(ACCOUNTING_SERVICE_NAME, SET_POST_STATUS_METHOD_NAME, parameters),
				SET_POST_STATUS_MESSAGE_TYPE);
			
			ServiceManager.request(url, 
				ACCOUNTING_SERVICE_NAME, SET_POST_STATUS_METHOD_NAME, 
				new Object[] {postStatus});
			
			registerDebugLog(
				received(ACCOUNTING_SERVICE_NAME, SET_POST_STATUS_METHOD_NAME, parameters, "OK"),
				SET_POST_STATUS_MESSAGE_TYPE);
			
		} catch(Throwable t) {
			
			registerErrorLog(
				fail(ACCOUNTING_SERVICE_NAME, SET_POST_STATUS_METHOD_NAME, parameters, t.getMessage()),
				SET_POST_STATUS_MESSAGE_TYPE);
			
			throw t;
		}
	}

	public void resetAllPostCounting() {
		
	}
	
	
	protected void registerDebugLog(String message, int type) {
		logger.debug(message);
	}
	
	protected void registerErrorLog(String message, int type) {
		logger.error(message);
	}
	
	protected String sending(String serviceName, String methodName, Object[] parameters) {
		return "Call remote method " + methodName + " of service " + serviceName + " with parameters [" + objectArrayToString(parameters) + "]";
	}
	
	protected String received(String serviceName, String methodName, Object[] parameters, String response) {
		return "Received response from method " + methodName + " of service " + serviceName + ": " + response;
	}
	
	protected String fail(String serviceName, String methodName, Object[] parameters, String fail) {
		return "Fail during call method " + methodName + " of service " + serviceName + ": " + fail;
	}
	
	
	private String[] convertParams(Object[][] matriz) {
		
		String[] params = new String[matriz.length];
		
		int index = 0;
		for(Object[] row : matriz) {
			params[index] = "[";
			for(Object param : row)
				params[index] += (params[index].length() > 1 ? ";" : "") + param.toString();
			params[index] += "]";
			index++;
		}
		
		return params;
	}
	
	private String objectArrayToString(Object[] parameters) {
		
		String strParams = "";
		
		if(parameters != null && parameters.length > 0)
			for(Object param : parameters)
				strParams += (!strParams.isEmpty() ? ";" : "") + param.toString();
		
		return strParams;
	}
}
