package br.com.transire.octopus.middleware.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.ScrollPaneConstants;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

public class MainView extends JFrame {

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -6109678300361239175L;
	
	
	protected JTabbedPane tbMain;
	
	protected JLabel lbSocketConfigInfo;
	
	protected JLabel lbSocketStatus;
	
	protected JTabbedPane tbSocket;
	
	protected JTextArea txaSocketDestinationLog;
	
	protected JToggleButton tbtSocketScrollMode;
	
	private int socketLogScrollValue = 0;
	
	protected JLabel lbCLPMemConfigInfo;
	
	protected JLabel lbCLPMemStatus;
	
	protected JCheckBox ckbCLPMemShowReadTimes;
	
	protected JButton btCLPMemTest;
	
	protected JTabbedPane tbCLPMem;
	
	protected JTextArea txaCLPMemSeparationLog;
	
	protected JTextArea txaCLPMemChangeBoxLog;
	
	protected JTextArea txaCLPMemPostStatusLog;
	
	protected JTextArea txaCLPMemPostCapacityLog;
	
	protected JToggleButton tbtCLPMemScrollMode;
	
	private int clpMemSeparationLogScrollValue = 0;
	
	private int clpMemChangeBoxLogScrollValue = 0;
	
	private int clpMemPostStatusLogScrollValue = 0;
	
	private int clpMemPostCapacityLogScrollValue = 0;
	
	protected JButton btCLPMemReconnect;
	
	protected JLabel lbOctWSConfigInfo;
	
	protected JLabel lbOctWSStatus;

	protected JTabbedPane tbOctWS;
	
	protected JTextArea txaOctWSDestinationLog;
	
	protected JTextArea txaOctWSConfirmationLog;
	
	protected JTextArea txaOctWSResetPostLog;
	
	protected JTextArea txaOctWSPostStatusLog;
	
	protected JToggleButton tbtOctWSScrollMode;
	
	private int octWSDestinationLogScrollValue = 0;
	
	private int octWSConfirmationLogScrollValue = 0;
	
	private int octWSResetPostLogScrollValue = 0;
	
	private int octWSPostStatusLogScrollValue = 0;
	
	
	private static final Color BACKGROUND_COLOR_DEFAULT = new Color(32, 67, 102);
	
	private static final Color FOREGROUND_COLOR_DEFAULT = new Color(102, 255, 204);
	
	
	public MainView() {
		
		setSize(800, 600);
		setLocationRelativeTo(null);
		
		setTitle("OCTOPUS - Middleware v.1.0.3");
		
		setIconImage(new ImageIcon("icon/octopus_icon_16.png").getImage());
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setLayout(new BorderLayout(5, 5));
		
		JPanel pnMain = new JPanel(new BorderLayout(5, 5));
		add(pnMain, BorderLayout.CENTER);
		pnMain.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		tbMain = new JTabbedPane();
		pnMain.add(tbMain, BorderLayout.CENTER);
		
		
		// Socket
		
		JPanel pnSocket = new JPanel();
		tbMain.addTab("Socket", pnSocket);
		
		pnSocket.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		pnSocket.setLayout(new FormLayout(new ColumnSpec[] {
				
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("25px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,}));

		Box bxSocketConfigs = Box.createHorizontalBox();
		
		lbSocketConfigInfo = new JLabel(" ");
		bxSocketConfigs.add(lbSocketConfigInfo);
		
		bxSocketConfigs.add(Box.createHorizontalGlue());
		
		pnSocket.add(bxSocketConfigs, "2, 2, 1, 1, fill, fill");
		
		tbSocket = new JTabbedPane(JTabbedPane.BOTTOM);
		pnSocket.add(tbSocket, "2, 4, 1, 1, fill, fill");
		
		JPanel pnSocketDestination = new JPanel();
		tbSocket.addTab("Destination", pnSocketDestination);
		
		pnSocketDestination.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));
		pnSocketDestination.setLayout(new BorderLayout());
		
		txaSocketDestinationLog = new JTextArea();
		txaSocketDestinationLog.setEditable(false);
		txaSocketDestinationLog.setWrapStyleWord(true);
		txaSocketDestinationLog.setLineWrap(true);
		txaSocketDestinationLog.setFont(new Font("Times New Roma", Font.BOLD, 10));
		txaSocketDestinationLog.setForeground(FOREGROUND_COLOR_DEFAULT);
		txaSocketDestinationLog.setBackground(BACKGROUND_COLOR_DEFAULT);
		
		JScrollPane spSocketDestinationLog = new JScrollPane(txaSocketDestinationLog);
		spSocketDestinationLog.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnSocketDestination.add(spSocketDestinationLog, BorderLayout.CENTER);
		
		spSocketDestinationLog.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
	        public void adjustmentValueChanged(AdjustmentEvent e) { 
	        	if(tbtSocketScrollMode != null && tbtSocketScrollMode.isSelected())
	            	e.getAdjustable().setValue(socketLogScrollValue);
	        	else e.getAdjustable().setValue(e.getAdjustable().getMaximum());
	        }
	    });
		
		Box bxSocketActions = Box.createHorizontalBox();
		
		lbSocketStatus = new JLabel(" ");
		lbSocketStatus.setFont(new Font("Arial", Font.BOLD, 18));
		bxSocketActions.add(lbSocketStatus);
		
		bxSocketActions.add(Box.createHorizontalGlue());
		
		JButton btSocketCLearLog = new JButton("Clear");
		bxSocketActions.add(btSocketCLearLog);
		btSocketCLearLog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txaSocketDestinationLog.setText("");
			}
		});
		
		bxSocketActions.add(Box.createHorizontalStrut(5));
		
		tbtSocketScrollMode = new JToggleButton("#");
		tbtSocketScrollMode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				socketLogScrollValue = spSocketDestinationLog.getVerticalScrollBar().getValue();
			}
		});
		tbtSocketScrollMode.setMargin(new Insets(2, 4, 2, 4));
		bxSocketActions.add(tbtSocketScrollMode);
		
		pnSocket.add(bxSocketActions, "2, 6, 1, 1, default, default");
		
		
		// CLP Memory
		
		JPanel pnCLPMem = new JPanel();
		tbMain.addTab("CLP Memory", pnCLPMem);
		
		pnCLPMem.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		pnCLPMem.setLayout(new FormLayout(new ColumnSpec[] {
				
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("25px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,}));

		Box bxCLPMemConfigs = Box.createHorizontalBox();
		
		lbCLPMemConfigInfo = new JLabel(" ");
		bxCLPMemConfigs.add(lbCLPMemConfigInfo);
		
		bxCLPMemConfigs.add(Box.createHorizontalGlue());
		
		ckbCLPMemShowReadTimes = new JCheckBox("Display Reading Times");
		ckbCLPMemShowReadTimes.setHorizontalTextPosition(JCheckBox.LEFT);
		bxCLPMemConfigs.add(ckbCLPMemShowReadTimes);
		
		bxCLPMemConfigs.add(Box.createHorizontalStrut(5));
		
		btCLPMemTest = new JButton("T");
		//bxCLPMemConfigs.add(btCLPMemTest);
		
		pnCLPMem.add(bxCLPMemConfigs, "2, 2, 1, 1, fill, fill");
		
		tbCLPMem = new JTabbedPane(JTabbedPane.BOTTOM);
		pnCLPMem.add(tbCLPMem, "2, 4, 1, 1, fill, fill");
		
		JPanel pnCLPMemSeparation = new JPanel();
		tbCLPMem.addTab("Separations", pnCLPMemSeparation);
		
		pnCLPMemSeparation.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));
		pnCLPMemSeparation.setLayout(new BorderLayout());
		
		txaCLPMemSeparationLog = new JTextArea();
		txaCLPMemSeparationLog.setEditable(false);
		txaCLPMemSeparationLog.setWrapStyleWord(true);
		txaCLPMemSeparationLog.setLineWrap(true);
		txaCLPMemSeparationLog.setFont(new Font("Times New Roma", Font.BOLD, 10));
		txaCLPMemSeparationLog.setForeground(FOREGROUND_COLOR_DEFAULT);
		txaCLPMemSeparationLog.setBackground(BACKGROUND_COLOR_DEFAULT);
		
		JScrollPane spCLPMemSeparationLog = new JScrollPane(txaCLPMemSeparationLog);
		spCLPMemSeparationLog.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnCLPMemSeparation.add(spCLPMemSeparationLog, BorderLayout.CENTER);
		
		spCLPMemSeparationLog.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
	        public void adjustmentValueChanged(AdjustmentEvent e) {  
	            if(tbtCLPMemScrollMode != null && tbtCLPMemScrollMode.isSelected())
	            	e.getAdjustable().setValue(clpMemSeparationLogScrollValue);
	        	else e.getAdjustable().setValue(e.getAdjustable().getMaximum());
	        }
	    });
		
		JPanel pnCLPMemChangeBox = new JPanel();
		tbCLPMem.addTab("Change Box", pnCLPMemChangeBox);
		
		pnCLPMemChangeBox.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));
		pnCLPMemChangeBox.setLayout(new BorderLayout());
		
		txaCLPMemChangeBoxLog = new JTextArea();
		txaCLPMemChangeBoxLog.setEditable(false);
		txaCLPMemChangeBoxLog.setWrapStyleWord(true);
		txaCLPMemChangeBoxLog.setLineWrap(true);
		txaCLPMemChangeBoxLog.setFont(new Font("Times New Roma", Font.BOLD, 10));
		txaCLPMemChangeBoxLog.setForeground(FOREGROUND_COLOR_DEFAULT);
		txaCLPMemChangeBoxLog.setBackground(BACKGROUND_COLOR_DEFAULT);
		
		JScrollPane spCLPMemChangeBoxLog = new JScrollPane(txaCLPMemChangeBoxLog);
		spCLPMemChangeBoxLog.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnCLPMemChangeBox.add(spCLPMemChangeBoxLog, BorderLayout.CENTER);
		
		spCLPMemChangeBoxLog.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
	        public void adjustmentValueChanged(AdjustmentEvent e) {  
	        	if(tbtCLPMemScrollMode != null && tbtCLPMemScrollMode.isSelected())
	            	e.getAdjustable().setValue(clpMemChangeBoxLogScrollValue);
	        	else e.getAdjustable().setValue(e.getAdjustable().getMaximum());
	        }
	    });
		
		JPanel pnCLPMemPostStatus = new JPanel();
		tbCLPMem.addTab("Post Status", pnCLPMemPostStatus);
		
		pnCLPMemPostStatus.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));
		pnCLPMemPostStatus.setLayout(new BorderLayout());
		
		txaCLPMemPostStatusLog = new JTextArea();
		txaCLPMemPostStatusLog.setEditable(false);
		txaCLPMemPostStatusLog.setWrapStyleWord(true);
		txaCLPMemPostStatusLog.setLineWrap(true);
		txaCLPMemPostStatusLog.setFont(new Font("Times New Roma", Font.BOLD, 10));
		txaCLPMemPostStatusLog.setForeground(FOREGROUND_COLOR_DEFAULT);
		txaCLPMemPostStatusLog.setBackground(BACKGROUND_COLOR_DEFAULT);
		
		JScrollPane spCLPMemPostStatusLog = new JScrollPane(txaCLPMemPostStatusLog);
		spCLPMemPostStatusLog.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnCLPMemPostStatus.add(spCLPMemPostStatusLog, BorderLayout.CENTER);
		
		spCLPMemPostStatusLog.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
	        public void adjustmentValueChanged(AdjustmentEvent e) {  
	        	if(tbtCLPMemScrollMode != null && tbtCLPMemScrollMode.isSelected())
	            	e.getAdjustable().setValue(clpMemPostStatusLogScrollValue);
	        	else e.getAdjustable().setValue(e.getAdjustable().getMaximum());  
	        }
	    });
		
		// Post Capacity
		
		JPanel pnCLPMemPostCapacity = new JPanel();
		tbCLPMem.addTab("Post Capacity", pnCLPMemPostCapacity);
		
		pnCLPMemPostCapacity.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));
		pnCLPMemPostCapacity.setLayout(new BorderLayout());
		
		txaCLPMemPostCapacityLog = new JTextArea();
		txaCLPMemPostCapacityLog.setEditable(false);
		txaCLPMemPostCapacityLog.setWrapStyleWord(true);
		txaCLPMemPostCapacityLog.setLineWrap(true);
		txaCLPMemPostCapacityLog.setFont(new Font("Times New Roma", Font.BOLD, 10));
		txaCLPMemPostCapacityLog.setForeground(FOREGROUND_COLOR_DEFAULT);
		txaCLPMemPostCapacityLog.setBackground(BACKGROUND_COLOR_DEFAULT);
		
		JScrollPane spCLPMemPostCapacityLog = new JScrollPane(txaCLPMemPostCapacityLog);
		spCLPMemPostCapacityLog.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnCLPMemPostCapacity.add(spCLPMemPostCapacityLog, BorderLayout.CENTER);
		
		spCLPMemPostCapacityLog.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
	        public void adjustmentValueChanged(AdjustmentEvent e) {  
	            if(tbtCLPMemScrollMode != null && tbtCLPMemScrollMode.isSelected())
	            	e.getAdjustable().setValue(clpMemPostCapacityLogScrollValue);
	        	else e.getAdjustable().setValue(e.getAdjustable().getMaximum());
	        }
	    });
		
		Box bxCLPMemActions = Box.createHorizontalBox();
		
		lbCLPMemStatus = new JLabel(" ");
		lbCLPMemStatus.setFont(new Font("Arial", Font.BOLD, 18));
		bxCLPMemActions.add(lbCLPMemStatus, "2, 2, 1, 1, center, center");
		
		bxCLPMemActions.add(Box.createHorizontalGlue());
		
		btCLPMemReconnect = new JButton("Reconnect");
		bxCLPMemActions.add(btCLPMemReconnect);
		
		bxCLPMemActions.add(Box.createHorizontalStrut(5));
				
		JButton btCLPMemCLearLog = new JButton("Clear");
		bxCLPMemActions.add(btCLPMemCLearLog);
		btCLPMemCLearLog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txaCLPMemSeparationLog.setText("");
				txaCLPMemChangeBoxLog.setText("");
				txaCLPMemPostStatusLog.setText("");
			}
		});
		
		bxCLPMemActions.add(Box.createHorizontalStrut(5));
		
		tbtCLPMemScrollMode = new JToggleButton("#");
		tbtCLPMemScrollMode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clpMemSeparationLogScrollValue = spCLPMemSeparationLog.getVerticalScrollBar().getValue();
				clpMemChangeBoxLogScrollValue = spCLPMemChangeBoxLog.getVerticalScrollBar().getValue();
				clpMemPostStatusLogScrollValue = spCLPMemPostStatusLog.getVerticalScrollBar().getValue();
			}
		});
		tbtCLPMemScrollMode.setMargin(new Insets(2, 4, 2, 4));
		bxCLPMemActions.add(tbtCLPMemScrollMode);
		
		pnCLPMem.add(bxCLPMemActions, "2, 6, 1, 1, default, default");
		
		
		// OCTOPUS Web Service
		
		JPanel pnOctWS = new JPanel();
		tbMain.addTab("Octopus WS", pnOctWS);
		
		pnOctWS.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		pnOctWS.setLayout(new FormLayout(new ColumnSpec[] {
				
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("25px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,}));

		Box bxOctWSConfigs = Box.createHorizontalBox();
		
		lbOctWSConfigInfo = new JLabel(" ");
		bxOctWSConfigs.add(lbOctWSConfigInfo);
		
		bxOctWSConfigs.add(Box.createHorizontalGlue());
		
		pnOctWS.add(bxOctWSConfigs, "2, 2, 1, 1, fill, fill");
		
		tbOctWS = new JTabbedPane(JTabbedPane.BOTTOM);
		pnOctWS.add(tbOctWS, "2, 4, 1, 1, fill, fill");
		
		JPanel pnOctWSDestination = new JPanel();
		tbOctWS.addTab("Destination", pnOctWSDestination);
		
		pnOctWSDestination.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));
		pnOctWSDestination.setLayout(new BorderLayout());
		
		txaOctWSDestinationLog = new JTextArea();
		txaOctWSDestinationLog.setEditable(false);
		txaOctWSDestinationLog.setWrapStyleWord(true);
		txaOctWSDestinationLog.setLineWrap(true);
		txaOctWSDestinationLog.setFont(new Font("Times New Roma", Font.BOLD, 10));
		txaOctWSDestinationLog.setForeground(FOREGROUND_COLOR_DEFAULT);
		txaOctWSDestinationLog.setBackground(BACKGROUND_COLOR_DEFAULT);
		
		JScrollPane spOctWSDestinationLog = new JScrollPane(txaOctWSDestinationLog);
		spOctWSDestinationLog.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnOctWSDestination.add(spOctWSDestinationLog, BorderLayout.CENTER);
		
		spOctWSDestinationLog.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
	        public void adjustmentValueChanged(AdjustmentEvent e) {  
	            if(tbtOctWSScrollMode != null && tbtOctWSScrollMode.isSelected())
	            	e.getAdjustable().setValue(octWSDestinationLogScrollValue);
	        	else e.getAdjustable().setValue(e.getAdjustable().getMaximum());
	        }
	    });
		
		JPanel pnOctWSConfirmation = new JPanel();
		tbOctWS.addTab("Confirmation", pnOctWSConfirmation);
		
		pnOctWSConfirmation.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));
		pnOctWSConfirmation.setLayout(new BorderLayout());
		
		txaOctWSConfirmationLog = new JTextArea();
		txaOctWSConfirmationLog.setEditable(false);
		txaOctWSConfirmationLog.setWrapStyleWord(true);
		txaOctWSConfirmationLog.setLineWrap(true);
		txaOctWSConfirmationLog.setFont(new Font("Times New Roma", Font.BOLD, 10));
		txaOctWSConfirmationLog.setForeground(FOREGROUND_COLOR_DEFAULT);
		txaOctWSConfirmationLog.setBackground(BACKGROUND_COLOR_DEFAULT);
		
		JScrollPane spOctWSConfirmationLog = new JScrollPane(txaOctWSConfirmationLog);
		spOctWSConfirmationLog.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnOctWSConfirmation.add(spOctWSConfirmationLog, BorderLayout.CENTER);
		
		spOctWSConfirmationLog.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
	        public void adjustmentValueChanged(AdjustmentEvent e) {  
	        	if(tbtOctWSScrollMode != null && tbtOctWSScrollMode.isSelected())
	            	e.getAdjustable().setValue(octWSConfirmationLogScrollValue);
	        	else e.getAdjustable().setValue(e.getAdjustable().getMaximum());  
	        }
	    });
		
		JPanel pnOctWSResetPost = new JPanel();
		tbOctWS.addTab("Reset Post", pnOctWSResetPost);
		
		pnOctWSResetPost.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));
		pnOctWSResetPost.setLayout(new BorderLayout());
		
		txaOctWSResetPostLog = new JTextArea();
		txaOctWSResetPostLog.setEditable(false);
		txaOctWSResetPostLog.setWrapStyleWord(true);
		txaOctWSResetPostLog.setLineWrap(true);
		txaOctWSResetPostLog.setFont(new Font("Times New Roma", Font.BOLD, 10));
		txaOctWSResetPostLog.setForeground(FOREGROUND_COLOR_DEFAULT);
		txaOctWSResetPostLog.setBackground(BACKGROUND_COLOR_DEFAULT);
		
		JScrollPane spOctWSResetPostLog = new JScrollPane(txaOctWSResetPostLog);
		spOctWSResetPostLog.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnOctWSResetPost.add(spOctWSResetPostLog, BorderLayout.CENTER);
		
		spOctWSResetPostLog.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
	        public void adjustmentValueChanged(AdjustmentEvent e) {  
	        	if(tbtOctWSScrollMode != null && tbtOctWSScrollMode.isSelected())
	            	e.getAdjustable().setValue(octWSResetPostLogScrollValue);
	        	else e.getAdjustable().setValue(e.getAdjustable().getMaximum());
	        }
	    });
		
		JPanel pnOctWSPostStatus = new JPanel();
		tbOctWS.addTab("Post Status", pnOctWSPostStatus);
		
		pnOctWSPostStatus.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));
		pnOctWSPostStatus.setLayout(new BorderLayout());
		
		txaOctWSPostStatusLog = new JTextArea();
		txaOctWSPostStatusLog.setEditable(false);
		txaOctWSPostStatusLog.setWrapStyleWord(true);
		txaOctWSPostStatusLog.setLineWrap(true);
		txaOctWSPostStatusLog.setFont(new Font("Times New Roma", Font.BOLD, 10));
		txaOctWSPostStatusLog.setForeground(FOREGROUND_COLOR_DEFAULT);
		txaOctWSPostStatusLog.setBackground(BACKGROUND_COLOR_DEFAULT);
		
		JScrollPane spOctWSPostStatusLog = new JScrollPane(txaOctWSPostStatusLog);
		spOctWSPostStatusLog.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnOctWSPostStatus.add(spOctWSPostStatusLog, BorderLayout.CENTER);
		
		spOctWSPostStatusLog.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
	        public void adjustmentValueChanged(AdjustmentEvent e) {  
	        	if(tbtOctWSScrollMode != null && tbtOctWSScrollMode.isSelected())
	            	e.getAdjustable().setValue(octWSPostStatusLogScrollValue);
	        	else e.getAdjustable().setValue(e.getAdjustable().getMaximum());
	        }
	    });
		
		Box bxOctWSActions = Box.createHorizontalBox();
		
		lbOctWSStatus = new JLabel(" ");
		lbOctWSStatus.setFont(new Font("Arial", Font.BOLD, 18));
		bxOctWSActions.add(lbOctWSStatus, "2, 2, 1, 1, center, center");
		
		bxOctWSActions.add(Box.createHorizontalGlue());
		
		JButton btOctWSCLearLog = new JButton("Clear");
		bxOctWSActions.add(btOctWSCLearLog);
		btOctWSCLearLog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txaOctWSDestinationLog.setText("");
				txaOctWSConfirmationLog.setText("");
				txaOctWSResetPostLog.setText("");
				txaOctWSPostStatusLog.setText("");
			}
		});
		
		bxOctWSActions.add(Box.createHorizontalStrut(5));
		
		tbtOctWSScrollMode = new JToggleButton("#");
		tbtOctWSScrollMode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				octWSDestinationLogScrollValue = spOctWSDestinationLog.getVerticalScrollBar().getValue();
				octWSConfirmationLogScrollValue = spOctWSConfirmationLog.getVerticalScrollBar().getValue();
				octWSResetPostLogScrollValue = spOctWSResetPostLog.getVerticalScrollBar().getValue();
				octWSPostStatusLogScrollValue = spOctWSPostStatusLog.getVerticalScrollBar().getValue();
			}
		});
		tbtOctWSScrollMode.setMargin(new Insets(2, 4, 2, 4));
		bxOctWSActions.add(tbtOctWSScrollMode);
		
		pnOctWS.add(bxOctWSActions, "2, 6, 1, 1, default, default");
	}
	
	public static void main(String[] args) {
		
		MainView window = new MainView();
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				window.setVisible(true);
			}
		});
	}
}
