package br.com.transire.octopus.middleware.socket;

public interface MessageHandler {

	public String handleMessage(String clientHost, String message);
}
