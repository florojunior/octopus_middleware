package br.com.transire.octopus.middleware.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import br.com.transire.octopus.middleware.client.OctopusWSClient;
import br.com.transire.octopus.middleware.clp.CLPMemoryAccess;
import br.com.transire.octopus.middleware.util.TimeIndicators;

public class CLPMemoryAccessController {

	
	private static final Logger logger = LogManager.getLogger(CLPMemoryAccessController.class.getSimpleName());
	
	
	public static final int SEPARATION_MESSAGE_TYPE = 1;
	
	public static final int CHANGE_BOX_MESSAGE_TYPE = 2;
	
	public static final int POST_STATUS_MESSAGE_TYPE = 3;
	
	public static final int POST_CAPACITY_MESSAGE_TYPE = 4;
	
	
	private static final String SEPARATION_MEMORY_TAG_NAME = "SR_String_NF_Time";
	
	private static final String CHANGE_BOX_MEMORY_TAG_NAME = "Est_Reset_Time";
	
	private static final String POST_STATUS_MEMORY_TAG_NAME = "Est_Ativado_String";
	
	private static final String POST_COUNT_TAG_NAME = "Est_Quantidade_Acc";
	
	private static final String POST_LIMIT_TAG_NAME = "Est_Quantidade_Pre";
	
	
	private static final SimpleDateFormat clpDateFormat = new SimpleDateFormat("yyyy/MM/dd-HH:mm:ss.SSSS");
	
	private static final int READING_TIMES_SAMPLE_DEFAULT = 10;
	
	
	private String clpIPAddress;
	
	private int cicleTime;
	
	private CLPMemoryAccess clpMemoryAccess;
	
	private Timer readMemoryTimer;
	
	private OctopusWSClient octopusClient;
	
	private boolean registerReadingTimesInLog = false;
	
	
	private Thread sendSeparationThread;
	
	private Thread sendChangeBoxThread;
	
	private Thread sendPostStatusThread;
	
	
	private Map<Integer, Separation[]> separationMap;
	
	private Map<Integer, Date> changeBoxMap;
	
	private Map<Integer, Boolean> postStatusMap;
	
	
	private List<Separation> newSeparations;
	
	private List<Integer> newChangeBoxes;
	
	private List<PostStatus> newPostStatus;
	
	
	private TimeIndicators separationsTimeIndicators;
	
	private TimeIndicators changeBoxTimeIndicators;
	
	private TimeIndicators postStatusTimeIndicators;
	
	
	public CLPMemoryAccessController(String clpIPAddress, int cicleTime, OctopusWSClient octopusClient) {
		
		this.clpIPAddress = clpIPAddress;
		this.cicleTime = cicleTime;
		this.octopusClient = octopusClient;
		
		this.clpMemoryAccess = new CLPMemoryAccess(clpIPAddress, 0);
		
		this.separationMap = new HashMap<Integer, Separation[]>();
		this.changeBoxMap = new HashMap<Integer, Date>();
		this.postStatusMap = new HashMap<Integer, Boolean>();
		
		this.newSeparations = Collections.synchronizedList(new ArrayList<Separation>());
		this.newChangeBoxes = Collections.synchronizedList(new ArrayList<Integer>());
		this.newPostStatus = Collections.synchronizedList(new ArrayList<PostStatus>());
		
		this.separationsTimeIndicators = new TimeIndicators(READING_TIMES_SAMPLE_DEFAULT);
		this.changeBoxTimeIndicators = new TimeIndicators(READING_TIMES_SAMPLE_DEFAULT);
		this.postStatusTimeIndicators = new TimeIndicators(READING_TIMES_SAMPLE_DEFAULT);
	}
	
	
	/**
	 * Inicia o processo de controle da leitura das memorias da CLP
	 */
	public void start() {
		
		try {
			
			clpMemoryAccess.connect();
			
			connected(clpIPAddress, 0);
			
			// inicia o ciclo de leitura de memorias
			initReadMemory(cicleTime);
			
			// inicia a thread que envia as separacoes para o octopus
			startSendNewSeparationsProcess();
			
			// inicia a thread que envia as trocas de caixas para o octopus
			startSendNewChangeBoxProcess();
			
			// inicia a thread que envia os status dos postos para o octopus
			startSendPostStatusBoxProcess();
			
		} catch (Exception e) {
			failConnection(clpIPAddress, e.getClass().getSimpleName() + ": " + e.getMessage());
		}
	}
	
	public void initReadMemory(int cicleTime) {
		
		if(readMemoryTimer != null)
			readMemoryTimer.cancel();
		
		readMemoryTimer = new Timer();
		
		readMemoryTimer.schedule(new TimerTask() {
			public void run() {
				
				readSeparationMemory();
				
				readChangeBoxMemory();
				
				readPostStatusMemory();
			}
		}, cicleTime, cicleTime);
	}
	
	private void startSendNewSeparationsProcess() {
		
		if(sendSeparationThread != null && sendSeparationThread.isAlive())
			sendSeparationThread.interrupt();
			
		sendSeparationThread = new Thread(new Runnable() {
			public void run() {
				
				while(true) {
					
					// se nao houver nenhum dado novo para enviar a thread aguarda uma notificacao
					synchronized(newSeparations) {
						
						if(newSeparations.isEmpty())
							try {
								newSeparations.wait();
							} catch (InterruptedException e) {}
					}
					
					sendNewSeparationsToOctopus();
				}
			}
		});
		
		sendSeparationThread.start();
		
		registerDebugLog("Separation Send Process Started", SEPARATION_MESSAGE_TYPE);
	}
	
	private void startSendNewChangeBoxProcess() {
		
		if(sendChangeBoxThread != null && sendChangeBoxThread.isAlive())
			sendChangeBoxThread.interrupt();
			
		sendChangeBoxThread = new Thread(new Runnable() {
			public void run() {
				
				while(true) {
					
					// se nao houver nenhum dado novo para enviar a thread aguarda uma notificacao
					synchronized(newChangeBoxes) {
						
						if(newChangeBoxes.isEmpty())
							try {
								newChangeBoxes.wait();
							} catch (InterruptedException e) {}
					}
					
					sendNewChangeBoxesToOctopus();
				}
			}
		});
		
		sendChangeBoxThread.start();
		
		registerDebugLog("Change Box Send Process Started", CHANGE_BOX_MESSAGE_TYPE);
	}
	
	private void startSendPostStatusBoxProcess() {
		
		if(sendPostStatusThread != null && sendPostStatusThread.isAlive())
			sendPostStatusThread.interrupt();
			
		sendPostStatusThread = new Thread(new Runnable() {
			public void run() {
				
				while(true) {
					
					// se nao houver nenhum dado novo para enviar a thread aguarda uma notificacao
					synchronized(newPostStatus) {
						
						if(newPostStatus.isEmpty())
							try {
								newPostStatus.wait();
							} catch (InterruptedException e) {}
					}
					
					sendNewPostStatusToOctopus();
				}
			}
		});
		
		sendPostStatusThread.start();
		
		registerDebugLog("Post Status Send Process Started", POST_STATUS_MESSAGE_TYPE);
	}

	/**
	 * Executa a leitura da memoria de Confirmacoes de Separacao dos Pacotes
	 */
	private void readSeparationMemory() {
		
		try {
			
			long initTime = new Date().getTime();
			
			// Realiza a leitura da memoria na CLP
			String[][] data = clpMemoryAccess.read(SEPARATION_MEMORY_TAG_NAME, 80, 3);
			
			int postNumber = 1;
			
			Separation[] separations = null;
			Separation separation = null;
			String[] dataParts = null;
			
			// Converte os dados lidos para o formato Estruturado
			int count = 0;
			for(String[] col : data) {
				
				separations = new Separation[col.length];
				count = 0;
				
				for(String row : col) {
					
					separation = new Separation();
					separation.setPostNumber(postNumber);
					
					if(!row.isEmpty()) { 
						
						dataParts = row.split(";");
						
						if(dataParts.length > 0)
							separation.setPackageId(dataParts[0]);
						
						if(dataParts.length > 1) {
							
							try {
								separation.setDateTimeConfirmation(clpDateFormat.parse(dataParts[1]));
							} catch(ParseException e) {
								separation.setDateTimeConfirmation(null);
							}
						}
					}
					
					separations[count++] = separation;
				}
				
				// Se houver separacoes ja registradas compara com a nova leitura para identificar novos registros
				if(separationMap.containsKey(postNumber))
					addNewSeparation(compareSeparations(separationMap.get(postNumber), separations));
				
				// Atualiza a nova leitura no mapa de separacoes
				separationMap.put(postNumber++, separations);
			}
			
			long totalTime = new Date().getTime() - initTime;
			
			separationsTimeIndicators.addValue(totalTime);
			displaySeparationReadingTimesIndicators(separationsTimeIndicators.toString());
			
			if(registerReadingTimesInLog)
				registerDebugLog("CLP Tag Memory " + SEPARATION_MEMORY_TAG_NAME + 
					" read in " + totalTime + " ms", SEPARATION_MESSAGE_TYPE);
			
		} catch (Throwable t) {
			
			registerErrorLog("Read memory tag " + SEPARATION_MEMORY_TAG_NAME + 
				" Fail: " + t.getMessage(), SEPARATION_MESSAGE_TYPE);
		}
	}
	
	private static List<Separation> compareSeparations(Separation[] oldSeps, Separation[] newSeps) {
		
		List<Separation> diffs = new ArrayList<Separation>();
		
		boolean exists = false;
		for(Separation nSep : newSeps) {
			
			exists = false;
			
			if(oldSeps != null && oldSeps.length > 0)
				for(Separation oSep : oldSeps) 
					// compara as datas e horarios para nao mais atrelar ao codigo da NF que podem ser iguais
					if((nSep.getDateTimeConfirmation() == null && oSep.getDateTimeConfirmation() == null) 
							|| (nSep.getDateTimeConfirmation() != null && oSep.getDateTimeConfirmation() != null 
							&& nSep.getDateTimeConfirmation().compareTo(oSep.getDateTimeConfirmation()) == 0)) {
						exists = true;
						break;
					}
			
			if(!exists)
				diffs.add(nSep);
		}
		
		return diffs;
	}
	
	private void addNewSeparation(List<Separation> separations) {
		
		newSeparations.addAll(separations);
		
		// notifica a thread que enviara as novas informacoes para o octopus
		synchronized(newSeparations) {
			newSeparations.notifyAll();
		}
		
		if(separations != null && !separations.isEmpty())
			for(Separation separation : separations)
				registerDebugLog("New Separation Detected: PostNumber " + separation.getPostNumber() + 
					" - PackageId " + separation.getPackageId(), SEPARATION_MESSAGE_TYPE);
	}
	
	/**
	 * Executa a leitura das memorias de troca de caixas
	 */
	private void readChangeBoxMemory() {
		
		try {
			
			long initTime = new Date().getTime();
			
			// Realiza a leitura da memoria na CLP
			String[] data = clpMemoryAccess.read(CHANGE_BOX_MEMORY_TAG_NAME, 80);
			
			Date date = null;
			
			int postNumber = 1;
			for(String dt : data) {
				
				date = null;
				
				try {
					if(dt != null && !dt.isEmpty())
						date = clpDateFormat.parse(dt);
				} catch(ParseException e) {
					date = null;
				} finally {
					
					// verifica se ha diferencas entre a nova data lida e a data armazenada
					if(changeBoxMap.containsKey(postNumber) && 
						date != null && !date.equals(changeBoxMap.get(postNumber)))
						addNewChangeBox(postNumber);
							
					changeBoxMap.put(postNumber++, date);
				}
			}
			
			long totalTime = new Date().getTime() - initTime;
			
			changeBoxTimeIndicators.addValue(totalTime);
			displayChangeBoxReadingTimesIndicators(changeBoxTimeIndicators.toString());
			
			if(registerReadingTimesInLog)
				registerDebugLog("CLP Tag Memory " + CHANGE_BOX_MEMORY_TAG_NAME + 
					" read in " + totalTime + " ms", CHANGE_BOX_MESSAGE_TYPE);
			
		} catch (Throwable t) {
			
			registerErrorLog("Read memory tag " + CHANGE_BOX_MEMORY_TAG_NAME + 
				" Fail: " + t.getMessage(), CHANGE_BOX_MESSAGE_TYPE);
		}
	}
	
	public void addNewChangeBox(Integer postNumber) {
		
		newChangeBoxes.add(postNumber);
		
		// notifica a thread que enviara as novas informacoes para o octopus
		synchronized(newChangeBoxes) {
			newChangeBoxes.notifyAll();
		}
				
		registerDebugLog("New Change Box Detected: PostNumber " + postNumber, 
			CHANGE_BOX_MESSAGE_TYPE);
	}
	
	/**
	 * Executa a leitura da memoria de status de postos
	 */
	private void readPostStatusMemory() {
		
		try {
		
			long initTime = new Date().getTime();
			
			// Realiza a leitura da memoria na CLP
			String data = clpMemoryAccess.read(POST_STATUS_MEMORY_TAG_NAME);
			
			int postNumber = 1;
			Boolean status = null;
			
			for(int index = 0; index < data.length(); index++) {
				
				status = data.charAt(index) == '1';
				
				// verifica se ha mudanca no status novo para o status armazenado
				if(postStatusMap.containsKey(postNumber) 
						&& postStatusMap.get(postNumber) != status)
					addNewPostStatus(new PostStatus(postNumber, status));
				
				postStatusMap.put(postNumber++, status);
			}
			
			long totalTime = new Date().getTime() - initTime;
			
			postStatusTimeIndicators.addValue(totalTime);
			displayPostStatusReadingTimesIndicators(postStatusTimeIndicators.toString());
			
			if(registerReadingTimesInLog)
				registerDebugLog("CLP Tag Memory " + POST_STATUS_MEMORY_TAG_NAME + 
					" read in " + totalTime + " ms", POST_STATUS_MESSAGE_TYPE);
		
		} catch (Throwable t) {

			registerErrorLog("Read memory tag " + POST_STATUS_MEMORY_TAG_NAME + 
				" Fail: " + t.getMessage(), POST_STATUS_MESSAGE_TYPE);
		}
	}
	
	private void addNewPostStatus(PostStatus postStatus) {
		
		newPostStatus.add(postStatus);
		
		// notifica a thread que enviara as novas informacoes para o octopus
		synchronized(newPostStatus) {
			newPostStatus.notifyAll();
		}
		
		registerDebugLog("New Post Status Detected: PostNumber " + postStatus.getPostNumber() + 
			" - Active " + postStatus.getActive(), POST_STATUS_MESSAGE_TYPE);
	}
	
	/**
	 * Verifica se existem novas Separacoes para enviar para o Octopus atraves do Web Service
	 */
	private void sendNewSeparationsToOctopus() {
		
		if(newSeparations == null || newSeparations.isEmpty())
			return;
		
		try {
			
			long initTime = new Date().getTime();
			
			List<Separation> sent = new ArrayList<Separation>();
			
			List<String[]> data = new ArrayList<String[]>();
			for(Separation sep : newSeparations) {
				
				data.add(new String[] {
					sep.getPackageId(),
					sep.getPostNumber() != null ? sep.getPostNumber().toString() : "0"
				});
				
				sent.add(sep);
				
				registerDebugLog("New Separation Sent: PostNumber " + sep.getPostNumber() + 
					" - PackageId " + sep.getPackageId(), SEPARATION_MESSAGE_TYPE);
			}
		
			octopusClient.confirmPostReceipt(data.toArray(new String[data.size()][]));		
			
			// remove da lista de novas separacoes as que ja foram enviadas para o octopus
			for(Separation sep : sent)
				newSeparations.remove(sep);
			
			registerDebugLog("New Separations successfully sent in " + 
				(new Date().getTime() - initTime) + " ms", 
				SEPARATION_MESSAGE_TYPE);
			
		} catch(Throwable t) {
			registerErrorLog("Failed to send new Separation(s): " + t.getMessage(), SEPARATION_MESSAGE_TYPE);
		}		
	}
	
	private void sendNewChangeBoxesToOctopus() {
		
		if(newChangeBoxes == null || newChangeBoxes.isEmpty())
			return;
		
		try {
			
			long initTime = new Date().getTime();
			
			List<Integer> sent = new ArrayList<Integer>();
			
			for(Integer cb : newChangeBoxes) {
	
				sent.add(cb);
				
				registerDebugLog("New Change Box Sent: PostNumber " + cb, 
					CHANGE_BOX_MESSAGE_TYPE);
			}
					
			octopusClient.resetPostCounting(sent.toArray(new Integer[sent.size()]));		
			
			// remove da lista de novas trocas de caixa as que ja foram enviadas para o octopus
			for(Integer cb : sent)
				newChangeBoxes.remove(cb);
			
			registerDebugLog("New Change Boxes successfully sent in " + 
				(new Date().getTime() - initTime) + " ms", 
				CHANGE_BOX_MESSAGE_TYPE);
			
		} catch(Throwable t) {
			registerErrorLog("Failed to send new Change Box(es): " + t.getMessage(), CHANGE_BOX_MESSAGE_TYPE);
		}		
	}

	private void sendNewPostStatusToOctopus() {
		
		if(newPostStatus == null || newPostStatus.isEmpty())
			return;
		
		try {
			
			long initTime = new Date().getTime();
			
			List<PostStatus> sent = new ArrayList<PostStatus>();
			
			List<Integer[]> data = new ArrayList<Integer[]>();
			for(PostStatus ps : newPostStatus) {
				
				data.add(new Integer[] {
					ps.getPostNumber(),
					ps.getActive() ? 1 : 0
				});
				
				sent.add(ps);
				
				registerDebugLog("Send New Post Status: PostNumber " + ps.getPostNumber() + 
					" - Active " + ps.getActive(), POST_STATUS_MESSAGE_TYPE);
			}
		
			octopusClient.setPostStatus(data.toArray(new Integer[data.size()][]));		
			
			// remove da lista de novas separacoes as que ja foram enviadas para o octopus
			for(PostStatus ps : sent)
				newPostStatus.remove(ps);
		
			registerDebugLog("New Post Status successfully sent in " + 
				(new Date().getTime() - initTime) + " ms", 
				POST_STATUS_MESSAGE_TYPE);
			
		} catch(Throwable t) {
			registerErrorLog("Failed to send new Post Status: " + t.getMessage(), POST_STATUS_MESSAGE_TYPE);
		}		
	}

	/**
	 * Escreve na memoria da CLP para sinalizar que uma caixa esta cheia
	 * 
	 * @param boxNumber
	 */
	public void writePostCapacity(Integer postNumber, boolean full) {
		
		try {
			
			String postIndex = "[" + (postNumber - 1) + "]";
			
			// le o valor da memoria de contagem
			Integer valueAcc = clpMemoryAccess.readInt(POST_COUNT_TAG_NAME + postIndex);
			
			clpMemoryAccess.writeInt(POST_LIMIT_TAG_NAME + postIndex, full ? valueAcc : 50);
			
			registerDebugLog("Post " + postNumber + " Capacity change successfully to " + (full ? "full" : "free"), 
				POST_CAPACITY_MESSAGE_TYPE);
			
		} catch (Throwable t) {
			registerErrorLog("Failed to change Post " + postNumber + " Capacity to " + 
				(full ? "full" : "free") + ": " + t.getMessage(), POST_CAPACITY_MESSAGE_TYPE);
		}
	}
	
	/**
	 * Finaliza a Thread e os Recursos de leitura das Memorias
	 */
	public void finishMemoryRead() {
		
		try {
			
			if(readMemoryTimer != null)
				readMemoryTimer.cancel();
			
			if(sendSeparationThread != null && sendSeparationThread.isAlive())
				sendSeparationThread.interrupt();
			
			if(sendChangeBoxThread != null && sendChangeBoxThread.isAlive())
				sendChangeBoxThread.interrupt();
			
			if(sendPostStatusThread != null && sendPostStatusThread.isAlive())
				sendPostStatusThread.interrupt();
			
			clpMemoryAccess.disconnect();
			
		} catch (Exception e) {
			
		}
	}
		
	public boolean isRegisterReadingTimesInLog() {
		return registerReadingTimesInLog;
	}

	public void setRegisterReadingTimesInLog(boolean registerReadingTimesInLog) {
		this.registerReadingTimesInLog = registerReadingTimesInLog;
	}


	protected void connected(String clpIPAddress, int port) {}
	
	protected void failConnection(String clpIPAddress, String error) {}
	
	protected void displaySeparationReadingTimesIndicators(String indicators) {}
	
	protected void displayChangeBoxReadingTimesIndicators(String indicators) {}
	
	protected void displayPostStatusReadingTimesIndicators(String indicators) {}
	
	

	protected void registerDebugLog(String message, int type) {
		logger.debug(message);
	}
	
	protected void registerErrorLog(String message, int type) {
		logger.error(message);
	}
	
	
	// Classes
	
	
	protected static class Separation {
				
		private Integer postNumber;
		
		private String packageId;
		
		private Date dateTimeConfirmation;

		
		public Separation() {
		}
		
		public Separation(Integer postNumber, String packageId, Date dateTimeConfirmation) {
			this.postNumber = postNumber; 
			this.packageId = packageId;
			this.dateTimeConfirmation = dateTimeConfirmation;
		}
		
		
		public Integer getPostNumber() {
			return postNumber;
		}

		public void setPostNumber(Integer postNumber) {
			this.postNumber = postNumber;
		}

		public String getPackageId() {
			return packageId;
		}

		public void setPackageId(String packageId) {
			this.packageId = packageId;
		}

		public Date getDateTimeConfirmation() {
			return dateTimeConfirmation;
		}

		public void setDateTimeConfirmation(Date dateTimeConfirmation) {
			this.dateTimeConfirmation = dateTimeConfirmation;
		}
	}
	
	protected class PostStatus {
		
		private Integer postNumber;
		
		private Boolean active;

		
		public PostStatus(Integer postNumber, Boolean active) {
			this.postNumber = postNumber;
			this.active = active;
		}
		
		public Integer getPostNumber() {
			return postNumber;
		}

		public void setPostNumber(Integer postNumber) {
			this.postNumber = postNumber;
		}

		public Boolean getActive() {
			return active;
		}

		public void setActive(Boolean active) {
			this.active = active;
		}
	}
}
