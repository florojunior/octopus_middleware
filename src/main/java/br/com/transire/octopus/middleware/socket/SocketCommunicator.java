package br.com.transire.octopus.middleware.socket;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SocketCommunicator {

	
	private static final Logger logger = LogManager.getLogger(SocketCommunicator.class.getSimpleName());
	
	
	private ServerSocket serverSocket;
	
	private MessageHandler messageHandler;
	
	private Thread serverThread;
	
	private Thread communicationThread;
	
	
	public SocketCommunicator(int port, MessageHandler messageHandler) throws IOException {
		
		this.messageHandler = messageHandler;
		
		this.serverSocket = new ServerSocket(port);
		
		registerDebugLog("Server Started and Listening on Port " + port);
		serverStarted(port);
	}
	
	public void startServer() {
		
		serverThread = new Thread(new Runnable() {
			public void run() {
				
				try {
					
					while(true)
						initComunication(serverSocket.accept());
					
				} catch(IOException e) {
					registerErrorLog("Communication Stablishment Fail: " + e.getMessage());
				}
			}
		});
		
		serverThread.start();
	}
	
	private void initComunication(Socket client) {
		
		communicationThread = new Thread(new Runnable() {
			public void run() {
				
				String clientHost = client.getInetAddress().getHostAddress();
				
				registerDebugLog("Communication established with " + clientHost);
				
				try {
					
					Scanner receivedData = new Scanner(client.getInputStream());
					PrintStream sentData = new PrintStream(client.getOutputStream());
					
					String message = null;
					String response = null;
					
		            while(receivedData.hasNextLine()) {
		            	
		            	message = receivedData.nextLine();
		                
		                response = messageHandler.handleMessage(clientHost, message);
		                
		                sentData.write(response.getBytes());
		                sentData.flush();
		            }
	            
		            receivedData.close();
		            sentData.close();
					client.close();
					
				} catch(IOException e) {
					registerErrorLog("Communication with " + clientHost + " Fail: " + e.getMessage());
				}
				
				registerDebugLog("Communication finished with " + clientHost);
				communicationFinished(clientHost);
			}
		});
		
		communicationThread.start();
	}
	
	public void finishServer() {
		
		try {
			
			communicationThread.interrupt();
			serverThread.interrupt();
			
			if(serverSocket != null && !serverSocket.isClosed())
				serverSocket.close();
			
		} catch (IOException e) {
			registerErrorLog("Server finished with error: " + e.getMessage());
		}
	}
	
	protected void registerDebugLog(String message) {
		logger.debug(message);
	}
	
	protected void registerErrorLog(String message) {
		logger.error(message);
	}

	protected void serverStarted(int port) {}
	
	protected void communicationStablished(String clientHost) {}
	
	protected void communicationFinished(String clientHost) {}
}
