package br.com.transire.octopus.middleware.util;

import java.util.ArrayList;
import java.util.List;

public class TimeIndicators {

	
	private int sample;
	
	private List<Integer> values;
	
	private int avg;
	
	private int min;
	
	private int max;

	
	public TimeIndicators(int sample) {
		
		this.sample = sample;
		this.values = new ArrayList<Integer>();
		this.avg = 0;
		this.min = 0;
		this.max = 0;
	}

	
	public void addValue(Number value) {
		
		// se o tamanho da lista for igual a amostra remove o primeiro valor
		if(values.size() >= sample)
			values.remove(0);
		
		values.add(value.intValue());
		
		calculate();
	}
	
	private void calculate() {
		
		int total = 0;
		
		min = values.get(0);
		max = values.get(0);
		
		for(Integer val : values) {
			
			total += val;
			
			if(val < min)
				min = val;
			
			if(val > max)
				max = val;
		}
		
		avg = total / values.size();
	}
	
	public int getAvg() {
		return avg;
	}

	public int getMin() {
		return min;
	}

	public int getMax() {
		return max;
	}
	
	@Override
	public String toString() {
		return 
			"LAST: " + (values.size() > 0 ? values.get(values.size() - 1) : 0) +
			"  |  MIN: " + min + "  |  MAX: " + max + "  |  AVG: " + avg + "  (ms)";
	}
}
