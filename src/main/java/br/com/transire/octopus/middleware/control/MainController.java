package br.com.transire.octopus.middleware.control;

import java.awt.EventQueue;

import br.com.transire.octopus.middleware.view.MainViewAction;

public class MainController {

	public static void main(String[] args) {
		
		MainViewAction window = new MainViewAction();
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				window.setVisible(true);
			}
		});
	}
}
