package br.com.transire.octopus.middleware.util;

public class TextUtil {

	public static int countLines(String text) {
		
		int lineCount = 0;
		
		if(text == null || text.isEmpty())
			return lineCount;
		
		for(int index = 0; index < text.length(); index++)
			if(text.charAt(index) == '\n')
				lineCount++;
	
		return lineCount;
	}
	
	public static String removeFirstLines(String text, int numberOfLines) {
		
		if(text == null || text.isEmpty())
			return text;
		
		int countLines = 0;
		int cutPosition = 0;
		for(int index = 0; index < text.length() && countLines < numberOfLines; index++)
			if(text.charAt(index) == '\n') {
				countLines++;
				cutPosition = index;
			}
		
		if(countLines >= numberOfLines)
			return text.substring(cutPosition + 1);
			
		return text;
	}
	
	public static void main(String[] args) {
		
		String text = 
			"magnoluz Sancelmo 01\n" +
			"magnoluz Sancelmo 02\n" +
			"magnoluz Sancelmo 03\n" +
			"magnoluz Sancelmo 04\n" +
			"magnoluz Sancelmo 05\n" +
			"magnoluz Sancelmo 06\n" +
			"magnoluz Sancelmo 07\n" +
			"magnoluz Sancelmo 08\n" +
			"magnoluz Sancelmo 09\n" +
			"magnoluz Sancelmo 10\n" +
			"magnoluz Sancelmo 11\n" +
			"magnoluz Sancelmo 12\n" +
			"magnoluz Sancelmo 13\n" +
			"magnoluz Sancelmo 14\n" +
			"magnoluz Sancelmo 15\n" +
			"magnoluz Sancelmo 16\n" +
			"magnoluz Sancelmo 17\n" +
			"magnoluz Sancelmo 18\n" +
			"magnoluz Sancelmo 19\n" +
			"magnoluz Sancelmo 20\n";
		
		System.out.println("linhas " + countLines(text));
		
		System.out.println(removeFirstLines(text, 2));
	}
}
