package br.com.transire.octopus.middleware.clp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import etherip.EtherNetIP;
import etherip.types.CIPData;
import etherip.types.CIPData.Type;

public class CLPMemoryAccess {

	
	private static final int LIMIT_READ_TAGS_PER_TIME = 5;
	
	
	private String ipAddress;
	
	private int slot;
	
	private EtherNetIP plc;
	
	
	public CLPMemoryAccess(String ipAddress, int slot) {
		this.ipAddress = ipAddress;
		this.slot = slot;	
	}
	
	
	public void connect() throws Exception {
		plc = new EtherNetIP(ipAddress, slot);
		plc.connectTcp();
	}
	
	public int getPort() throws Exception {
		return plc.getConnectionData().getOriginatorPort();
	}
	
	public String read(String tagName) throws Exception {
		CIPData data = plc.readTag(tagName);
		return data != null ? data.getString() : null;
	}
	
	public Integer readInt(String tagName) throws Exception {
		CIPData data = plc.readTag(tagName);
		return data != null ? data.getNumber(0).intValue() : null;
	}
	
	public String[] read(String tagName, int cols) throws Exception {
		
		// Cria um array com o nome de todas as tags do vetor que devem ser lidas
		String[] tags = new String[cols];
		
		int col = 0;
		for(col = 0; col < cols; col++)
			tags[col] = tagName + "[" + col + "]";
		
		List<CIPData> data = new ArrayList<CIPData>();
		
		// Cria um array para conter parte das tags de acordo com o limite de leitura por vez
		String[] partTags = new String[
		    LIMIT_READ_TAGS_PER_TIME <= tags.length ? 
		    LIMIT_READ_TAGS_PER_TIME : tags.length];
		
		int index = 0;
		int maxCount = tags.length - 1;
		
		// executa a leitura conforme o limite de leitura de tags por vez
		for(int count = 0; count <= maxCount; count++) {
			
			partTags[index++] = tags[count];
			
			if(index == LIMIT_READ_TAGS_PER_TIME || count >= maxCount) {
				
				data.addAll(Arrays.asList(plc.readTags(partTags)));
				
				partTags = new String[
				    LIMIT_READ_TAGS_PER_TIME <= maxCount - count ? 
				    LIMIT_READ_TAGS_PER_TIME : maxCount - count];
				
				index = 0;
			}
		}
		
		String[] readValues = new String[cols];
		
		col = 0;
		for(CIPData cd : data)
			readValues[col++] = cd.getString();
		
		return readValues;
	}

	public String[][] read(String tagName, int cols, int rows) throws Exception {
		
		// Cria um array com o nome de todas as tags da matriz que devem ser lidas
		String[] tags = new String[cols * rows];
		
		int col = 0, row = 0, index = 0;
		for(col = 0; col < cols; col++)
			for(row = 0; row < rows; row++)
				tags[index++] = tagName + "[" + col + "]." + row;
		
		List<CIPData> data = new ArrayList<CIPData>();
		
		// Cria um array para conter parte das tags de acordo com o limite de leitura por vez
		String[] partTags = new String[
		    LIMIT_READ_TAGS_PER_TIME <= tags.length ? 
		    LIMIT_READ_TAGS_PER_TIME : tags.length];
		
		index = 0;
		int maxCount = tags.length - 1;
		
		// executa a leitura conforme o limite de leitura de tags por vez
		for(int count = 0; count <= maxCount; count++) {
			
			partTags[index++] = tags[count];
			
			if(index == LIMIT_READ_TAGS_PER_TIME || count >= maxCount) {
				
				data.addAll(Arrays.asList(plc.readTags(partTags)));
				
				partTags = new String[
				    LIMIT_READ_TAGS_PER_TIME <= maxCount - count ? 
				    LIMIT_READ_TAGS_PER_TIME : maxCount - count];
				
				index = 0;
			}
		}
		
		// Converte os valores lidos para um matriz de strings
		String[][] readValues = new String[cols][rows];
		
		col = 0; 
		row = 0;
		for(CIPData cd : data) {
			readValues[col][row++] = cd != null ? cd.getString() : null;
			if(row == rows) {
				row = 0;
				col++;
			}
		}
		
		return readValues;
	}

	public void writeInt(String tag, Integer value) throws Exception {
		
		CIPData data = new CIPData(Type.DINT, 1);
		data.set(0, value);
		
		plc.writeTag(tag, data);
	}
	
	public boolean isConnected() {
		
		boolean yes = false;
		
		if(plc != null)
			try {
				plc.getConnectionData();
				yes = true;
			} catch(Exception e) {
				yes = false;
			}
		
		return yes;
	}
	
	public void disconnect() throws Exception {
		if(plc != null)
    		plc.close();
	}
}
