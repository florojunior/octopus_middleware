package br.com.transire.octopus.middleware.core;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import br.com.transire.octopus.middleware.client.OctopusWSClient;
import br.com.transire.octopus.middleware.socket.MessageHandler;
import br.com.transire.octopus.middleware.util.TimeIndicators;

public class CLPSocketMessageHandler implements MessageHandler {

	
	private static final Logger logger = LogManager.getLogger(CLPSocketMessageHandler.class.getSimpleName());
	
	
	
	private OctopusWSClient octopusClient;
	
	private CLPMemoryAccessController clpMemoryAccessController;
	
	private int treadmillTotalTime;
	
	private TimeIndicators socketTimeIndicators;
	
	
	public CLPSocketMessageHandler(OctopusWSClient octopusClient, CLPMemoryAccessController clpMemoryAccessController, int treadmillTotalTime) {
		this.octopusClient = octopusClient;
		this.clpMemoryAccessController = clpMemoryAccessController;
		this.treadmillTotalTime = treadmillTotalTime;
		this.socketTimeIndicators = new TimeIndicators(10);
	}
	
	
	@Override
	public String handleMessage(String clientHost, String message) {
		
		if(message == null || message.isEmpty())
			return "0";
		
		long initTime = new Date().getTime();
		
		registerDebugLog(clientHost + " sent message '" + message.trim() + "'");
		
		// Os parametros estarao concatenados pelo caracter ';'
		String[] parameters = message.trim().split(";");
		
		// remover os espacos em branco que possa existir nos parametros
		if(parameters.length > 0)
			for(int index = 0; index < parameters.length; index++)
				parameters[index] = parameters[index].trim();
			
		// o primeiro parametro sera o identificador do pacote
		String packageId = parameters[0];
		
		String response = null;
		long totalTime = 0l;
		try {
			
			// a resposta devera seguir o protocolo 'Numero_Posto;Flag_Lotacao'
			response = octopusClient.determinateDestination(parameters);
			
			response = analyseResponse(response);
			
			totalTime = new Date().getTime() - initTime;
			
		} catch(Throwable t) {
			
			// Quando ocorrer algum erro envia para a caixa de erro '0'
			response = "0;0";
			
			registerErrorLog("Message Handle Fail: " + t.getMessage());
			
		} finally {
			
			socketTimeIndicators.addValue(totalTime);
			displaySocketDestinationTimeIndicators(socketTimeIndicators.toString());
			
			registerDebugLog("Response to " + clientHost + " '" + packageId + ";" + response + 
				"' (time = " + totalTime + " ms)");
		}
		
		return packageId + ";" + response;
	}
	
	private String analyseResponse(String params) {
		
		String[] parts = params.split(";");
		
		if(parts.length < 2)
			throw new IllegalArgumentException("Parametros de Resposta Incorretos.");
		
		if(parts.length > 2) {
			
			Integer postNumber, signalImmediately = null;
			
			try {
				
				// O terceiro paramentro sera o numero do posto que deve ser sinalizado como cheio
				postNumber = Integer.parseInt(parts[2]);
				
				// O quarto parametro indica se a sinalizacao deve ser imediata ou deve esperar o tempo padrao
				signalImmediately = Integer.parseInt(parts[3]);
				
				if(signalImmediately == 0) {
					
					new Timer().schedule(new TimerTask() {
						public void run() {
							clpMemoryAccessController.writePostCapacity(postNumber, true);
							registerDebugLog("Post " + postNumber + " capacity change to full");
						}
					}, treadmillTotalTime);
					
					registerDebugLog("Post " + postNumber + " capacity will change to full in " + treadmillTotalTime + " ms");
					
				} else {
					
					clpMemoryAccessController.writePostCapacity(postNumber, true);
					registerDebugLog("Post " + postNumber + " capacity change to full");
				}
				
			} catch(NumberFormatException e) {
				registerErrorLog("Post Capacity parameters error: " + parts[2] + ";" + parts[3]);
			}
			
			params = parts[0] + ";" + parts[1];
		}
		
		return params;
	}
	
	protected void displaySocketDestinationTimeIndicators(String indicators) {}
	
	protected void registerDebugLog(String message) {
		logger.debug(message);
	}
	
	protected void registerErrorLog(String message) {
		logger.error(message);
	}
}
