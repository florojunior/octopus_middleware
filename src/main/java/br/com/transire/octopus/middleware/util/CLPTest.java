package br.com.transire.octopus.middleware.util;

import etherip.EtherNetIP;
import etherip.types.CIPData;

public class CLPTest {

	public static void main(String[] args) {
		
		EtherNetIP plc = new EtherNetIP("172.18.100.201", 0);
			
		try {
		
			plc.connectTcp();
			
			CIPData dataAcc = plc.readTag("Est_Quantidade_Acc[0]");
			
			
			//System.out.println("Value Pre = " + pre);
			//System.out.println(pre.toString().getBytes().length);
			
			//Integer val = 49;
			
			//CIPData data2 = new CIPData(Type.DINT, new Integer("50").toByteArray());
			plc.writeTag("Est_Quantidade_Pre[0]", dataAcc);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			if(plc  != null)
				try {
					plc.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
	}
}
